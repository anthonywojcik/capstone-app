class CreateVehicles < ActiveRecord::Migration
  def change
    create_table :vehicles do |t|
      t.integer :year
      t.string :make
      t.string :model
      t.decimal :GVW
      t.string :license
      t.decimal :miles
      t.string :motor
      t.string :VIN
      t.references :customer, index: true

      t.timestamps
    end
  end
end
