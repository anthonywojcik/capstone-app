class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.string :note
      t.references :vehicle, index: true
      t.references :TicketStatus, index: true

      t.timestamps
    end
  end
end
