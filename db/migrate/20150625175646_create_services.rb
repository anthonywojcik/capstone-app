class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :service
      t.string :position
      t.string :type
      t.references :category, index: true

      t.timestamps
    end
  end
end
