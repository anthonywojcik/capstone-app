class CreateTicketStatuses < ActiveRecord::Migration
  def change
    create_table :ticket_statuses do |t|
      t.string :status
      t.string :description

      t.timestamps
    end
  end
end
