json.array!(@vehicles) do |vehicle|
  json.extract! vehicle, :id, :year, :make, :model, :GVW, :license, :miles, :motor, :VIN, :customer_id
  json.url vehicle_url(vehicle, format: :json)
end
