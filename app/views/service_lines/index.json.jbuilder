json.array!(@service_lines) do |service_line|
  json.extract! service_line, :id, :hour, :note, :Ticket_id, :Service_id, :ServiceStatus_id, :Employee_id, :Part_id
  json.url service_line_url(service_line, format: :json)
end
