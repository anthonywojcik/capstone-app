class Service < ActiveRecord::Base
  has_many :service_lines
  belongs_to :category
end
