class Ticket < ActiveRecord::Base
  belongs_to :vehicle
  belongs_to :ticket_status
  has_many :service_lines
end
